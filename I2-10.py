from elasticsearch import Elasticsearch
import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta
import calendar

es=Elasticsearch(
    ['http://analytics.i.nextunicorn.kr/'],
    http_auth=('halfz','lsfkr@#$'),
    scheme="http",
    port=8080
)

accessDate=input("사용자 분석용 ! 어떤 월을 원하시나요? ?(yy/mm)>")
transDate=datetime.datetime.strptime("20"+accessDate,"%Y%m")
transDateMinusOne=(transDate-relativedelta(months=1)).strftime("%Y%m")
transDateMinusTwo=(transDate-relativedelta(months=2)).strftime("%Y%m")

qureyIndex="access-20"+accessDate[0:2]+"-"+accessDate[2:4]
qureyIndexMinusOne="access-20"+transDateMinusOne[2:4]+"-"+transDateMinusOne[4:6]
qureyIndexMinusTwo="access-20"+transDateMinusTwo[2:4]+"-"+transDateMinusTwo[4:6]



lastDay=calendar.monthrange(int("20"+accessDate[0:2]),int(accessDate[2:4]))[1]
lastDayMinusOne=calendar.monthrange(int(transDateMinusOne[0:4]),int(transDateMinusOne[4:6]))[1]
lastDayMinusTwo=calendar.monthrange(int(transDateMinusTwo[0:4]),int(transDateMinusTwo[4:6]))[1]


qureyParam={
    "size": 0,
    "aggs": {
        "views": {
            "cardinality": {
                "field": "userId"
            }
        }
    }
}

queryRes=es.search(index=qureyIndex,body=qureyParam)
monthlyRes=queryRes['aggregations']['views']['value']

qureyParamMinusOne={
    "size": 0,
    "aggs": {
        "views": {
            "cardinality": {
                "field": "userId"
            }
        }
    }
}
queryRes=es.search(index=qureyIndexMinusOne,body=qureyParamMinusOne)
monthlyResMinusOne=queryRes['aggregations']['views']['value']

qureyParamMinusTwo={
    "size": 0,
    "aggs": {
        "views": {
            "cardinality": {
                "field": "userId"
            }
        }
    }
}

queryRes=es.search(index=qureyIndexMinusTwo,body=qureyParamMinusTwo)
monthlyResMinusTwo=queryRes['aggregations']['views']['value']

qureyParamIntersectionOne = {
    "size": 0,
    "query": {
        "range": {
            "timestamp": {
                "gte": transDateMinusOne[0:4] + "-" + transDateMinusOne[4:6] + "-01",
                "lte": "20" + accessDate[0:2] + "-" + accessDate[2:4] + "-" + str(lastDay)
            }
        }
    },
    "aggs": {
        "views": {
            "cardinality": {
                "field": "userId"
            }
        }
    }
}
queryRes = es.search(index=(qureyIndex,qureyIndexMinusOne), body=qureyParamIntersectionOne)
monthlyIntersetionOne = queryRes['aggregations']['views']['value']

qureyParamIntersectionBeforeOne = {
    "size": 0,
    "query": {
        "range": {
            "timestamp": {
                "gte": transDateMinusTwo[0:4] + "-" + transDateMinusTwo[4:6] + "-01",
                "lte":  transDateMinusOne[0:4] + "-" + transDateMinusOne[4:6] + "-"+str(lastDayMinusOne)
            }
        }
    },
    "aggs": {
        "views": {
            "cardinality": {
                "field": "userId"
            }
        }
    }
}
queryRes = es.search(index=qureyIndexMinusTwo+","+qureyIndexMinusOne, body=qureyParamIntersectionBeforeOne)
monthlyIntersetionBeforeOne = queryRes['aggregations']['views']['value']

qureyParamIntersectionBeforeTwo = {
    "size": 0,
    "query": {
        "range": {
            "timestamp": {
                "gte": transDateMinusTwo[0:4] + "-" + transDateMinusTwo[4:6] + "-01",
                "lte": "20" + accessDate[0:2] + "-" + accessDate[2:4] + "-" + str(lastDay)
            }
        }
    },
    "aggs": {
        "views": {
            "cardinality": {
                "field": "userId"
            }
        }
    }
}
queryRes = es.search(index=qureyIndexMinusTwo+","+qureyIndex, body=qureyParamIntersectionBeforeTwo)
monthlyIntersetionBeforeTwo = queryRes['aggregations']['views']['value']

qureyParamIntersectionAll = {
    "size": 0,
    "query": {
        "range": {
            "timestamp": {
                "gte": transDateMinusTwo[0:4] + "-" + transDateMinusTwo[4:6] + "-01",
                "lte": "20" + accessDate[0:2] + "-" + accessDate[2:4] + "-" + str(lastDay)
            }
        }
    },
    "aggs": {
        "views": {
            "cardinality": {
                "field": "userId"
            }
        }
    }
}
queryRes = es.search(index=qureyIndexMinusTwo+","+qureyIndexMinusOne+","+qureyIndex, body=qureyParamIntersectionAll)
monthlyIntersetionAll = queryRes['aggregations']['views']['value']

inter1=monthlyRes+monthlyResMinusOne-monthlyIntersetionOne
inter2=monthlyResMinusOne+monthlyResMinusTwo-monthlyIntersetionBeforeOne
inter3=monthlyResMinusTwo+monthlyRes-monthlyIntersetionBeforeTwo
interAll=monthlyIntersetionAll-monthlyRes-monthlyResMinusOne-monthlyResMinusTwo+inter1+inter2+inter3


print(accessDate[0:2]+"년 "+accessDate[2:4]+"월에 접속한 사용자 수:",monthlyRes)
print(transDateMinusOne[2:4] + "년 " + transDateMinusOne[4:6] +"월에 접속한 사용자 수:",monthlyResMinusOne)
print(transDateMinusTwo[2:4] + "년 " + transDateMinusTwo[4:6] +"월에 접속한 사용자 수:",monthlyResMinusTwo)

print("최근 3개월동안 계속 이용한 사용자 수:",interAll)
print(accessDate[0:2]+"년 "+accessDate[2:4]+"월에 신규로 가입한 사용자 수:",monthlyRes-inter1-inter3+interAll)
print(transDateMinusOne[2:4] + "년 " + transDateMinusOne[4:6] +"월에 사용하지 않았지만 "+accessDate[0:2]+"년 "+accessDate[2:4]+"월에 다시 사용한 사용자 수:",inter3-interAll)
