#leo의 task i2-2
#last commit:2020/09/15
from elasticsearch import Elasticsearch

es=Elasticsearch(
    ['http://analytics.i.nextunicorn.kr/'],
    http_auth=('halfz','lsfkr@#$'),
    scheme="http",
    port=8080
)

accessDate=input("원하는 일자?(yy/mm/dd)>")
qureyIndex="access-20"+accessDate[0:2]+"-"+accessDate[2:4]
qureyParam={
    "size": 0,
     "query": {
        "range": {
            "timestamp": {
                "gte": "20"+accessDate[0:2]+"-"+accessDate[2:4]+"-"+accessDate[4:6],
                "lte": "20"+accessDate[0:2]+"-"+accessDate[2:4]+"-"+accessDate[4:6]
            }
        }
    },
    "aggs": {
        "views": {
            "cardinality": {
                "field": "userId"
            }
        }
    }
}

dailyCountRes=es.search(index=qureyIndex,body=qureyParam)
countRes=dailyCountRes['aggregations']['views']['value']

print(accessDate[0:2]+"년 "+accessDate[2:4]+"월 "+accessDate[4:6]+"의 접속 사용자 수:",countRes)