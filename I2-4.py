from elasticsearch import Elasticsearch
import datetime
from datetime import timedelta

es=Elasticsearch(
    ['http://analytics.i.nextunicorn.kr/'],
    http_auth=('halfz','lsfkr@#$'),
    scheme="http",
    port=8080
)

accessDate=input("원하는 연도와 주차?(yy/ww)>")
transDate=datetime.datetime.strptime("20"+accessDate+"-1","%Y%W-%w")
transDateStr=transDate.strftime("%Y%m%d")

countRes=0
for day in range(0,7):
    qureyIndex="access-20"+transDateStr[2:4]+"-"+transDateStr[4:6]
    qureyParam={
        "size": 0,
         "query": {
            "range": {
                "timestamp": {
                    "gte": "20"+transDateStr[2:4]+"-"+transDateStr[4:6]+"-"+transDateStr[6:8],
                    "lte": "20"+transDateStr[2:4]+"-"+transDateStr[4:6]+"-"+transDateStr[6:8]
                }
            }
        },
        "aggs": {
            "views": {
                "cardinality": {
                    "field": "userId"
                }
            }
        }
    }
    dailyCountRes=es.search(index=qureyIndex,body=qureyParam)
    countRes+=dailyCountRes['aggregations']['views']['value']

print(accessDate[0:2]+"년 "+accessDate[2:4]+"주차("+transDateStr+"~"+(transDate+timedelta(days=7)).strftime("%Y%m%d")+")에 접속한 사용자 수 :",countRes)