from elasticsearch import Elasticsearch

es=Elasticsearch(
    ['http://analytics.i.nextunicorn.kr/'],
    http_auth=('halfz','lsfkr@#$'),
    scheme="http",
    port=8080
)

accessDate=input("원하는 월의 사용자 ?(yy/mm)>")
qureyIndex="access-20"+accessDate[0:2]+"-"+accessDate[2:4]
qureyParam={
    "size": 0,
    "aggs": {
        "views": {
            "cardinality": {
                "field": "userId"
            }
        }
    }
}

dailyCountRes=es.search(index=qureyIndex,body=qureyParam)
countRes=dailyCountRes['aggregations']['views']['value']

print(accessDate[0:2]+"년 "+accessDate[2:4]+"월에 접속한 사용자 수:",countRes)