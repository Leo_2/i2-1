from elasticsearch import Elasticsearch
import pymysql
from datetime import datetime,timedelta


es=Elasticsearch(
    ['http://analytics.i.nextunicorn.kr/'],
    http_auth=('halfz','lsfkr@#$'),
    scheme="http",
    port=8080
)
conn=pymysql.connect(host='nu-data-flow-beta.cluster-cvevfntasot9.ap-northeast-2.rds.amazonaws.com',user='halfz',password='4ul0wVN49aHdJvWNZLBk',db='unicorns_access_log',charset='utf8')
curs=conn.cursor()
esLength=10000;
totalCount=0;
search_after=0;
#업데이트는 필요 없을까? 100개를 기준으로 할 떄 -> 1시간 40분 정도, 10000개 기준일때 -> 약 20분
#10/14일 기준 쿼리 7100000개 돌파!
while esLength>9999:#종료조건 -> 쿼리에서 부른 갯수보다 hit 된 쿼리의 길이가 작다면(timestamp가 존재하는 log 기준)
    print("#####"+str(totalCount)+"번쨰@@@@@@")
    qureyIndex="access-*-*"
    qureyParam={
        "size": 10000,#갯수!
        "_source": ["sessionId", "timestamp", "userId", "request.method", "request.path", "response.status","request.query.utm_source","request.query.utm_medium","request.query.utm_campaign"],
        "sort": [
            {"timestamp": "asc"}
        ]
    }
    if search_after!=0:
        qureyParam.update( {"search_after" : [search_after]})
    #쿼리파람을 expand 해야함 -> 처음에는 }만 붙이고, 다음번엔 쿼리조건에 "search_after" : [서치히트 숫자] 삽입 및 갱

    dailyLog=es.search(index=qureyIndex,body=qureyParam)

    sqlTuple=[]
    for i in range(0,len(dailyLog['hits']['hits'])):#갯수!
        if 'timestamp' in dailyLog['hits']['hits'][i]['_source']:
            if 'sessionId' in dailyLog['hits']['hits'][i]['_source']:
                sessionId=dailyLog['hits']['hits'][i]['_source']['sessionId']
            else:
                sessionId= None
            if 'fingerprint' in dailyLog['hits']['hits'][i]['_source']:
                fingerprint=dailyLog['hits']['hits'][i]['_source']['fingerprint']
            else:
                fingerprint = None
            if 'userId' in dailyLog['hits']['hits'][i]['_source']:
                user_id=dailyLog['hits']['hits'][i]['_source']['userId']
            else:
                user_id = None
            method=dailyLog['hits']['hits'][i]['_source']['request']['method']
            path=dailyLog['hits']['hits'][i]['_source']['request']['path']
            if 'response' in dailyLog['hits']['hits'][i]['_source']:
                if 'status' in dailyLog['hits']['hits'][i]['_source']['response']:
                    statusCode=dailyLog['hits']['hits'][i]['_source']['response']['status']
                else:
                    statusCode = None
            else:
                statusCode = None
            if 'query' in dailyLog['hits']['hits'][i]['_source']['request']:
                if 'utm_source' in dailyLog['hits']['hits'][i]['_source']['request']['query']:
                    utm_source= dailyLog['hits']['hits'][i]['_source']['request']['query']['utm_source']
                else:
                    utm_source=None
                if 'utm_medium' in dailyLog['hits']['hits'][i]['_source']['request']['query']:
                    utm_medium= dailyLog['hits']['hits'][i]['_source']['request']['query']['utm_medium']
                else:
                    utm_medium=None
                if 'utm_campaign' in dailyLog['hits']['hits'][i]['_source']['request']['query']:
                    utm_campaign= dailyLog['hits']['hits'][i]['_source']['request']['query']['utm_campaign']
                else:
                    utm_campaign=None
            else:
                utm_source = None
                utm_medium = None
                utm_campaign = None

            timestamp=dailyLog['hits']['hits'][i]['_source']['timestamp']
            timestamp=timestamp[0:10]+" "+timestamp[11:19]
            dateObj=datetime.strptime(timestamp,'%Y-%m-%d %H:%M:%S')+timedelta(hours=9)#time zone (ast +9 hour)
            timestamp=str(dateObj)

            search_after=dailyLog['hits']['hits'][i]['sort'][0]

            sqlTuple.append([sessionId, fingerprint, user_id, method, path, statusCode,utm_source,utm_medium,utm_campaign, timestamp])
    esLength=len(sqlTuple)
    #print(sqlTuple)
    #print(esLength)
    totalCount+=esLength
    sql="Insert into apiLog_session(sessionid, fingerprint, user_id, method, path, statusCode, utm_source, utm_medium, utm_campaign, createdAt) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
    curs.executemany(sql,sqlTuple)
    sqlTuple.clear()

conn.commit()

conn.close()
es.close()